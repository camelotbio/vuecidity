import Vue from 'vue'
import App from './app'
import Vuecidity from './vuecidity'

Vue.config.productionTip = false
Vue.use(Vuecidity)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})
