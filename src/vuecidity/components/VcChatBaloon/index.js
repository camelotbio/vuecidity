import VcChatBaloon from './VcChatBaloon'

VcChatBaloon.install = function install (Vue) {
  Vue.component(VcChatBaloon.name, VcChatBaloon)
}

export default VcChatBaloon
