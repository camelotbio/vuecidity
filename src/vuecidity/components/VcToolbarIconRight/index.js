import VcToolbarIconRight from './VcToolbarIconRight'

VcToolbarIconRight.install = function install (Vue) {
  Vue.component(VcToolbarIconRight.name, VcToolbarIconRight)
}

export default VcToolbarIconRight
