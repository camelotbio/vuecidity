import VcTag from './VcTag'

VcTag.install = function install (Vue) {
  Vue.component(VcTag.name, VcTag)
}

export default VcTag
