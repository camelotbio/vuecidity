import VcNotification from './VcNotification'

VcNotification.install = function install (Vue) {
  Vue.component(VcNotification.name, VcNotification)
}

export default VcNotification
