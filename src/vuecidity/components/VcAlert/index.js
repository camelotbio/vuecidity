import VcAlert from './VcAlert'

VcAlert.install = function install (Vue) {
  Vue.component(VcAlert.name, VcAlert)
}

export default VcAlert
