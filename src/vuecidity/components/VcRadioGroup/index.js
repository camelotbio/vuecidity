import VcRadioGroup from './VcRadioGroup'

VcRadioGroup.install = function install (Vue) {
  Vue.component(VcRadioGroup.name, VcRadioGroup)
}

export default VcRadioGroup
