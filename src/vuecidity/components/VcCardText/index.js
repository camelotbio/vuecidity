import VcCardText from './VcCardText'

VcCardText.install = function install (Vue) {
  Vue.component(VcCardText.name, VcCardText)
}

export default VcCardText
