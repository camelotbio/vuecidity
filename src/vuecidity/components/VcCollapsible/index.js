import VcCollapsible from './VcCollapsible'

VcCollapsible.install = function install (Vue) {
  Vue.component(VcCollapsible.name, VcCollapsible)
}

export default VcCollapsible
