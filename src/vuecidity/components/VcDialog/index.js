import VcDialog from './VcDialog'

VcDialog.install = function install (Vue) {
  Vue.component(VcDialog.name, VcDialog)
}

export default VcDialog
