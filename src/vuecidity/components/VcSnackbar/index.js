import VcSnackbar from './VcSnackbar'

VcSnackbar.install = function install (Vue) {
  Vue.component(VcSnackbar.name, VcSnackbar)
}

export default VcSnackbar
