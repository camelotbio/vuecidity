import VcTimeline from './VcTimeline'

VcTimeline.install = function install (Vue) {
  Vue.component(VcTimeline.name, VcTimeline)
}

export default VcTimeline
