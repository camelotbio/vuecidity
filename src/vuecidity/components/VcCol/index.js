import VcCol from './VcCol'

VcCol.install = function install (Vue) {
  Vue.component(VcCol.name, VcCol)
}

export default VcCol
