import VcCheckbox from './VcCheckbox'

VcCheckbox.install = function install (Vue) {
  Vue.component(VcCheckbox.name, VcCheckbox)
}

export default VcCheckbox
