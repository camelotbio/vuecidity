import VcListItemPrimaryText from './VcListItemPrimaryText'

VcListItemPrimaryText.install = function install (Vue) {
  Vue.component(VcListItemPrimaryText.name, VcListItemPrimaryText)
}

export default VcListItemPrimaryText
