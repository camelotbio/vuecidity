import VcNavDrawer from './VcNavDrawer'

VcNavDrawer.install = function install (Vue) {
  Vue.component(VcNavDrawer.name, VcNavDrawer)
}

export default VcNavDrawer
