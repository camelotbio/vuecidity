import VcSocialBar from './VcSocialBar'

VcSocialBar.install = function install (Vue) {
  Vue.component(VcSocialBar.name, VcSocialBar)
}

export default VcSocialBar
