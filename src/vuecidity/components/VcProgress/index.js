import VcProgress from './VcProgress'

VcProgress.install = function install (Vue) {
  Vue.component(VcProgress.name, VcProgress)
}

export default VcProgress
