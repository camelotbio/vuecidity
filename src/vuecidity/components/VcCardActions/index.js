import VcCardActions from './VcCardActions'

VcCardActions.install = function install (Vue) {
  Vue.component(VcCardActions.name, VcCardActions)
}

export default VcCardActions
