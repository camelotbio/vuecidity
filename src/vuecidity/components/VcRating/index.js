import VcRating from './VcRating'

VcRating.install = function install (Vue) {
  Vue.component(VcRating.name, VcRating)
}

export default VcRating
