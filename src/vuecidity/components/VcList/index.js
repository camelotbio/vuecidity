import VcList from './VcList'

VcList.install = function install (Vue) {
  Vue.component(VcList.name, VcList)
}

export default VcList
