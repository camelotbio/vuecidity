import VcChat from './VcChat'

VcChat.install = function install (Vue) {
  Vue.component(VcChat.name, VcChat)
}

export default VcChat
