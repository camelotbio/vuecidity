import VcToolbarControlsRight from './VcToolbarControlsRight'

VcToolbarControlsRight.install = function install (Vue) {
  Vue.component(VcToolbarControlsRight.name, VcToolbarControlsRight)
}

export default VcToolbarControlsRight
