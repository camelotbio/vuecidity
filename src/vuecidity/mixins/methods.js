const mixins = {
  methods: {
    /**
     * Create unique name by tab:name and pane:label
     * @method setName
     * @param {String} label [required]
     * @return {String}
     */
    setName (label) {
      if (label) {
        return `${this.name}-${label.replace(/\s+/g, '-')}`.toLowerCase()
      }
    },
    preventDefault (e) {
      e = e || window.event
      if (e.preventDefault) {
        e.preventDefault()
        e.returnValue = false
      }
    },
    preventDefaultForScrollKeys (e) {
      const keys = {
        37: 1,
        38: 1,
        39: 1,
        40: 1
      }
      if (keys[e.keyCode]) {
        this.preventDefault(e)
        return false
      }
    },
    disableScroll () {
      if (window.addEventListener) {
        window.addEventListener('DOMMouseScroll', this.preventDefault, false)
        window.onwheel = this.preventDefault
        window.onmousewheel = document.onmousewheel = this.preventDefault
        window.ontouchmove = this.preventDefault
        document.onkeydown = this.preventDefaultForScrollKeys
      }
    },
    enableScroll () {
      if (window.removeEventListener) {
        window.removeEventListener('DOMMouseScroll', this.preventDefault, false)
        window.onmousewheel = document.onmousewheel = null
        window.onwheel = null
        window.ontouchmove = null
        document.onkeydown = null
      }
    }
  }
}

export default mixins
