const mixins = {
  methods: {
    change () {
      const value = this.value
      if (this.checked.includes(value)) {
        this.checked.splice(this.checked.indexOf(value), 1)
      } else {
        this.checked.push(value)
      }
      this.triggerSwitch()
    },
    triggerSwitch () {
      // this.$emit('switch', this.value)
    }
  },
  props: {
    value: null,
    checked: null,
    disabled: {
      type: Boolean,
      required: false
    },
    id: {
      type: String,
      required: false
    },
    name: {
      type: [
        String,
        Number
      ],
      required: true
    },
    labelPosition: {
      type: String,
      required: false,
      default: 'right'
    },
    required: {
      type: Boolean,
      required: false
    },
    color: {
      type: String,
      required: false,
      default: 'grey'
    },
    dark: {
      type: Boolean,
      required: false,
      default: true
    }
  }
}

export default mixins
